#include <stdio.h>

int nrDigits(int num){
    static int count =0;

    if(num > 0){
      count++;
       nrDigits(num/10);
    }else{
      return count;
    }
}

int main(void){

      printf("%d count ",nrDigits(8965));
      return 0;
}
